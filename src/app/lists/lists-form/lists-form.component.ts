import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ListsService } from 'src/app/services/lists/lists.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-lists-form',
  templateUrl: './lists-form.component.html',
  styleUrls: ['./lists-form.component.css']
})
export class ListsFormComponent implements OnInit {

  formlists: FormGroup

  subscribe: Subscription

  idList: any = false

  idCategory: any

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    private lists: ListsService,
    private toastr: ToastrService
  ) { 
    this.formlists = formBuilder.group({
      id: [null],
      name: [null, Validators.required]
    })
  }

  ngOnInit() {
    this.idCategory = this.route.snapshot.paramMap.get('idCategory')
    this.idList = this.route.snapshot.paramMap.get('idList')
    if(this.idList){
      this.subscribe = this.lists.getListsById(this.idCategory, this.idList).subscribe((response:any)=>{
        this.formlists.patchValue({
          ...response
        })
      })
    }
  }

  ngOnDestroy(): void {
    if(this.subscribe){
      this.subscribe.unsubscribe()
    }
  }

  handlerSubmit() {
    if(this.formlists.valid){
      if(this.idList) {
        this.subscribe = this.lists.updateLists(this.idCategory ,this.formlists.value).subscribe((response:any)=>{
          this.toastr.success("Salvo com sucesso!");
          this.location.back();
        }, error => {
          this.toastr.error(error.error)
        })
      }else {
        this.subscribe = this.lists.postLists(this.idCategory , this.formlists.value).subscribe((response:any)=>{
          this.toastr.success("Salvo com sucesso!")
          this.location.back();
        }, error => {
          this.toastr.error(error.error)
        })
      }
    } 
  }

}
