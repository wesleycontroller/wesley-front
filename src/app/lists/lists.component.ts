import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ListsService } from '../services/lists/lists.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ConfirmationDialogService } from '../services/confirmation-dialog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {

  collection: any 

  subscribe: Subscription

  idCategory: any

  constructor(
    private lists: ListsService, 
    public router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private confirmationDialogService: ConfirmationDialogService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.idCategory = this.route.snapshot.paramMap.get('idCategory')
    this.getCollection()
  }

  ngOnDestroy(): void {
    if(this.subscribe) {
      this.subscribe.unsubscribe()
    }
  }

  getCollection() {
    this.subscribe = this.lists.getLists(this.idCategory).subscribe((response:any) => {
      this.collection = response
      console.log(this.collection)
    });
  }

  handleDelete(event:any) {
    this.confirmationDialogService.confirm('Confirme', 'Deseja realmente fazer isso?')
    .then((confirmed) => {
      if(confirmed) {
        this.lists.deleteLists(this.idCategory, event).subscribe((response:any) => {
          this.toastr.success("Excluido com sucesso!");
          this.getCollection()
        })
      }
    })
    .catch(() => console.log('Cacelado'));
  }

  back() {
    this.location.back();
  }

}
