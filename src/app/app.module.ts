import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoriesFormComponent } from './categories/categories-form/categories-form.component';
import { ListsComponent } from './lists/lists.component';
import { FormControlMessageComponent } from './shared/form-control-message/form-control-message.component';
import { TablesComponent } from './shared/tables/tables.component';
import { ListsFormComponent } from './lists/lists-form/lists-form.component';
import { ItemsFormComponent } from './items/items-form/items-form.component';
import { ItemsComponent } from './items/items.component';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './services/confirmation-dialog.service';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CategoriesComponent,
    CategoriesFormComponent,
    ListsComponent,
    FormControlMessageComponent,
    TablesComponent,
    ListsFormComponent,
    ItemsFormComponent,
    ItemsComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModalModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
	  ToastrModule.forRoot()
  ],
  entryComponents: [ConfirmationDialogComponent],
  providers: [ConfirmationDialogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
