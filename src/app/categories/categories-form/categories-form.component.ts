import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CategoriesService } from 'src/app/services/categories/categories.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.css']
})
export class CategoriesFormComponent implements OnInit {

  formCategory: FormGroup

  subscribe: Subscription

  idCategory: any = false

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    private categeries: CategoriesService,
    private toastr: ToastrService
  ) { 
    this.formCategory = formBuilder.group({
      id: [null],
      name: [null, Validators.required]
    })
  }

  ngOnInit() {
    this.idCategory = this.route.snapshot.paramMap.get('idCategory')
    if(this.idCategory){
      this.subscribe = this.categeries.getCategorieById(this.idCategory).subscribe((response:any)=>{
        this.formCategory.patchValue({
          ...response
        })
      })
    }
  }

  ngOnDestroy(): void {
    if(this.subscribe){
      this.subscribe.unsubscribe()
    }
  }

  handlerSubmit() {
    if(this.formCategory.valid){
      if(this.idCategory) {
        this.subscribe = this.categeries.updateCategories(this.formCategory.value).subscribe((response:any)=>{
          this.toastr.success("Salvo com sucesso!");
          this.location.back();
        }, error => {
          this.toastr.error(error.error)
        })
      }else {
        this.subscribe = this.categeries.postCategories(this.formCategory.value).subscribe((response:any)=>{
          this.toastr.success("Salvo com sucesso!")
          this.location.back();
        }, error => {
          this.toastr.error(error.error)
        })
      }
    } 
  }

}
