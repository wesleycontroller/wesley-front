import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../services/categories/categories.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ConfirmationDialogService } from '../services/confirmation-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  collection: any 

  subscribe: Subscription

  constructor(
    private category: CategoriesService, 
    public route: Router,
    private toastr: ToastrService,
    private confirmationDialogService: ConfirmationDialogService,
  ) { }

  ngOnInit() {
    this.getCollection()
  }

  ngOnDestroy(): void {
    if(this.subscribe) {
      this.subscribe.unsubscribe()
    }
  }

  getCollection() {
    this.subscribe = this.category.getCategories().subscribe((response:any) => {
      this.collection = response
    });
  }

  handleDelete(event:any) {
    this.confirmationDialogService.confirm('Confirme', 'Deseja realmente fazer isso?')
    .then((confirmed) => {
      if(confirmed) {
        this.category.deleteCategories(event).subscribe((response:any) => {
          this.toastr.success("Excluido com sucesso!");
          this.getCollection()
        })
      }
    })
    .catch(() => console.log('Cacelado'));
  }

}
