import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {

  @Input() collection: any[]

  @Input() nextLevel: String

  @Output() toDelete: EventEmitter<any> = new EventEmitter();

  constructor(private route: Router) { }

  ngOnInit() {
  }

  delete(item: any) {
    this.toDelete.emit(item)
  }

}
