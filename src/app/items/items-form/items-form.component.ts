import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ItemsService } from 'src/app/services/items/items.service';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-items-form',
  templateUrl: './items-form.component.html',
  styleUrls: ['./items-form.component.css']
})
export class ItemsFormComponent implements OnInit {

  formItem: FormGroup

  subscribe: Subscription

  idCategory: any

  idList: any

  idItem: any

  constructor(
    private formBuilder: FormBuilder, 
    private items: ItemsService,
    public router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
      this.formItem = formBuilder.group({
        id: [null],
        name: [null, [Validators.required, Validators.minLength(3)]],
        done: [false]
      })
   }

  ngOnInit() {
    this.idCategory = this.route.snapshot.paramMap.get('idCategory')
    this.idList = this.route.snapshot.paramMap.get('idList')
    this.idItem = this.route.snapshot.paramMap.get('idItem')

    if(this.idItem) {
      this.subscribe = this.items.getItemsById(this.idCategory, this.idList, this.idItem).subscribe((response:any)=>{
        this.formItem.patchValue({
          ...response
        })
      })
    }
  }

  ngOnDestroy(): void {
    if(this.subscribe){
      this.subscribe.unsubscribe()
    }
  }

  handlerSubmit() {
    if(this.formItem.valid) {
      if(this.idItem) {
        this.subscribe = this.items.ubpdateItems(this.idCategory, this.idList ,this.formItem.value).subscribe((response:any)=>{
          this.toastr.success("Salvo com sucesso!");
          this.location.back();
        }, error => {
          this.toastr.error(error.error)
        })
      }else{
        this.subscribe = this.items.postItems(this.idCategory, this.idList, this.formItem.value).subscribe((response:any) => {
          this.toastr.success("Salvo com sucesso!")
          this.location.back();
        }, error => {
          console.log(error)
          this.toastr.error(error.error)
        })
      }
    }
  }

  back() {
    this.location.back();
  }

}
