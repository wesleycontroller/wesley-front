import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ItemsService } from '../services/items/items.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ConfirmationDialogService } from '../services/confirmation-dialog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  collection: any 

  subscribe: Subscription

  idCategory: any

  idList: any

  constructor(
    private items: ItemsService, 
    public router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private confirmationDialogService: ConfirmationDialogService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.idCategory = this.route.snapshot.paramMap.get('idCategory')
    this.idList = this.route.snapshot.paramMap.get('idList')
    this.getCollection()
  }

  ngOnDestroy(): void {
    if(this.subscribe) {
      this.subscribe.unsubscribe()
    }
  }

  getCollection() {
    this.subscribe = this.items.getItems(this.idCategory, this.idList).subscribe((response:any) => {
      this.collection = response
      console.log(this.collection)
    });
  }

  handleDelete(event:any) {
    this.confirmationDialogService.confirm('Confirme', 'Deseja realmente fazer isso?')
    .then((confirmed) => {
      if(confirmed) {
        this.subscribe = this.items.deleteItems(this.idCategory, this.idList, event).subscribe((response:any) => {
          this.toastr.success("Excluido com sucesso!");
          this.getCollection()
        })
      }
    })
    .catch(() => console.log('Cacelado'));
  }

  handleDone(event:any, item:any) {
    let itemForEdit = {
      ...item,
      done: event.target.checked
    }
    this.subscribe = this.items.ubpdateItems(this.idCategory, this.idList, itemForEdit).subscribe((response:any) => {})
  }

  back() {
    this.location.back();
  }

}
