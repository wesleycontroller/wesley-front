import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent } from './categories/categories.component';
import { ListsComponent } from './lists/lists.component';
import { CategoriesFormComponent } from './categories/categories-form/categories-form.component';
import { ListsFormComponent } from './lists/lists-form/lists-form.component';
import { ItemsComponent } from './items/items.component';
import { ItemsFormComponent } from './items/items-form/items-form.component';


const routes: Routes = [
  {path: 'categories', component: CategoriesComponent },
  {path: 'categories/new', component: CategoriesFormComponent },
  {path: 'categories/:idCategory/edit', component: CategoriesFormComponent },
  {path: 'categories/:idCategory/lists', component: ListsComponent },
  {path: 'categories/:idCategory/lists/new', component: ListsFormComponent },
  {path: 'categories/:idCategory/lists/:idList/edit', component: ListsFormComponent },
  {path: 'categories/:idCategory/lists/:idList/items', component:  ItemsComponent},
  {path: 'categories/:idCategory/lists/:idList/items/new', component: ItemsFormComponent },
  {path: 'categories/:idCategory/lists/:idList/items/:idItem/edit', component: ItemsFormComponent },
  {path: '**', redirectTo: 'categories'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
